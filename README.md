# blockhosts [![pipeline status](https://gitlab.com/frickel/blockhosts/badges/master/pipeline.svg)](https://gitlab.com/frickel/blockhosts/commits/master)

Additional lists of hosts for my [pihole](https://github.com/pi-hole/) and IPs for my [Mikrotik](https://mikrotik.com/) firewall.


https://frickel.gitlab.io/blockhosts/hosts

https://frickel.gitlab.io/blockhosts/%C2%B5block.txt

https://frickel.gitlab.io/blockhosts/ips
